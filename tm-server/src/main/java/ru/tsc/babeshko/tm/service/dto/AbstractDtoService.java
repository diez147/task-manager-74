package ru.tsc.babeshko.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.babeshko.tm.api.service.dto.IDtoService;
import ru.tsc.babeshko.tm.dto.model.AbstractModelDto;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.repository.dto.AbstractDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDto> implements IDtoService<M> {

    @NotNull
    protected abstract AbstractDtoRepository<M> getRepository();

    @Override
    public void add(@NotNull final M model) {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.deleteAll();
        repository.saveAll(models);
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.save(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.delete(model);
    }

    @Override
    public void clear() {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.deleteAll();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        repository.deleteById(id);
    }

    @Override
    public long getCount() {
        @NotNull final AbstractDtoRepository<M> repository = getRepository();
        return repository.count();
    }

}