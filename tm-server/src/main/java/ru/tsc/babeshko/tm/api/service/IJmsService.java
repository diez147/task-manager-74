package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IJmsService {

    void send(@NotNull String message);

}

